#!/usr/bin/python
# coding: utf-8
import os
import json
import subprocess
from collections import namedtuple

import bottle


app = bottle.default_app()


SEMENTEIRA_AVAILABLE_CONTENT = (
    "autonomia",
    "sexualidade",
    "dicas",
    "tenologias-digitais",
    "saude",
    "agroecologia",
)


def get_upload_folder_dir():
    _upload_folder = os.environ.get("UPLOADFOLDER")
    if not (_upload_folder and os.path.exists(_upload_folder)):
        raise FileNotFoundError("No upload directory set.")
    return _upload_folder


def get_shared_files_content(filepath):
    _upload_folder = get_upload_folder_dir()
    if filepath:
        print("filepath: {}".format(filepath))
        if not os.path.exists(os.path.join(_upload_folder, filepath)):
            raise FileNotFoundError("No such file or directory.")

        if os.path.isfile(os.path.join(_upload_folder, filepath)):
            print("upload_path {} is a file".format(filepath))
            return bottle.static_file(filepath, root=_upload_folder)
        _upload_folder = os.path.join(_upload_folder, filepath)

    print("upload_path {} is dir".format(_upload_folder))
    with os.scandir(_upload_folder) as it:
        return sorted([entry.name for entry in it])


def save_file_content(upload):
    _upload_folder = get_upload_folder_dir()
    upload.save(_upload_folder)


def delete_file_content(filepath):
    _upload_folder = get_upload_folder_dir()
    path_to_delete = os.path.join(_upload_folder, filepath)
    if os.path.isfile(path_to_delete):
        print("path_to_delete {} is a file".format(filepath))
        os.remove(path_to_delete)


def rename_file(filepath, new_filename):
    _upload_folder = get_upload_folder_dir()
    print("filepath: {}".format(filepath))
    path_to_rename = os.path.join(_upload_folder, filepath)
    if not os.path.exists(path_to_rename):
        raise FileNotFoundError("No such file or directory.")
    dirname = os.path.dirname(path_to_rename)
    new_path = os.path.join(dirname, new_filename)
    os.rename(path_to_rename, new_path)


def get_disk_usage():
    _upload_folder = get_upload_folder_dir()
    _ntuple_diskusage = namedtuple('usage', 'total, used, free')
    st = os.statvfs(_upload_folder)
    return _ntuple_diskusage(
        st.f_blocks * st.f_frsize,
        (st.f_blocks - st.f_bfree) * st.f_frsize,
        st.f_bavail * st.f_frsize
    )


def make_install(properties):
    """
    sementeira == [] => none
    Se sucesso (arquivo lock), retornar
    """
    os.chdir("/opt/piratebox/bin")
    storage = {
        "sd": "SDCARD",
        "usb": "USB",
        "both": "BOTH",
    }
    command = [
        "SENHA_USUARIO=" + properties["sysKey"],
        "TIPO_ARMAZENAMENTO=" + storage[properties["storage"]],
    ]
    sementeira_list_size = len(properties["sementeira"])
    if sementeira_list_size == 0:
        command.append("CONTENT_SEMENTEIRA=none")
    elif sementeira_list_size == len(SEMENTEIRA_AVAILABLE_CONTENT):
        command.append("CONTENT_SEMENTEIRA=all")
    else:
        command.append("CONTENT_SEMENTEIRA=" + ",".join(properties["sementeira"]))

    if properties.get("netKey") is not None:
        command.append("SENHA_WIFI=" + properties["netKey"])
    if properties.get("skelentonKey") is not None:
        command.append("SENHA_ARQUIVOS=" + properties["skelentonKey"])
    command = command + ["HABILITAR_RODA=true", "bash", "install.sh"]
    subprocess.run(command, check=True)


@app.route("/hello", "GET")
def hello():
    return "Hello World!"


@app.route("/<filepath:path>", "DELETE")
def delete_file(filepath):
    try:
        delete_file_content(filepath)
    except FileNotFoundError as exc:
        msg = 'Could not delete shared file "/{}": {}'.format(
            (filepath or ""), str(exc)
        )
        bottle.abort(404, msg)
    else:
        bottle.response.content_type = "application/json"
        return bottle.HTTPResponse(status=204)


@app.route("/upload/", "POST")
def upload_file():
    try:
        upload = bottle.request.POST["upload"]
    except KeyError:
        bottle.abort(400, "upload field is mandatory.")
    else:
        try:
            save_file_content(upload)
        except FileNotFoundError as exc:
            msg = 'Could not save uploaded file "/Shared/{}": {}'.format(
                (upload.filename or ""), str(exc)
            )
            bottle.abort(404, msg)
        else:
            bottle.response.content_type = "application/json"
            return bottle.HTTPResponse(status=201)


@app.route("/rename/<filepath:path>", "PATCH")
def rename(filepath):
    try:
        new_filename = bottle.request.params["new_filename"]
    except KeyError:
        bottle.abort(400, "new_filename field is mandatory.")
    else:
        try:
            rename_file(filepath, new_filename)
        except FileNotFoundError as exc:
            msg = 'Could not rename file "{}": {}'.format(
                (filepath or ""), str(exc)
            )
            bottle.abort(404, msg)
        else:
            bottle.response.content_type = "application/json"
            return bottle.HTTPResponse(status=204)


@app.route("/Shared", "GET")
@app.route("/Shared/", "GET")
@app.route("/Shared/<filepath:path>", "GET")
def shared_files(filepath=None):
    try:
        _content = get_shared_files_content(filepath)
    except FileNotFoundError as exc:
        msg = 'Could not get shared file "/Shared/{}": {}'.format(
            (filepath or ""), str(exc)
        )
        bottle.abort(404, msg)
    else:
        if isinstance(_content, list):
            bottle.response.content_type = "application/json"
            return json.dumps(_content)
        return _content


@app.route("/disk-usage", "GET")
@app.route("/disk-usage/", "GET")
def disk_usage():
    try:
        _disk_info = get_disk_usage()
    except FileNotFoundError as exc:
        msg = "Could not get disk usage: {}".format(str(exc))
        bottle.abort(404, msg)
    else:
        bottle.response.content_type = "application/json"
        return json.dumps({
            "total": _disk_info.total,
            "used": _disk_info.used,
            "free": _disk_info.free,
        })


def validate_install_params(params):
    _validated = {}
    _missing_params = []
    for _mandatory_param in ("sementeira", "sysKey", "storage"):
        value = params.getall(_mandatory_param)
        if len(value) == 0:
            _missing_params.append(_mandatory_param)
        elif len(value) == 1:
            _validated[_mandatory_param] = value[0]
        else:
            _validated[_mandatory_param] = value

    if len(_missing_params) > 0:
        raise ValueError("mandatory params: {}".format(_missing_params))

    _invalid_content = [
        content
        for content in _validated["sementeira"]
        if content not in SEMENTEIRA_AVAILABLE_CONTENT
    ]
    if len(_invalid_content) > 0:
        raise ValueError("{} isn't a valid sementeira content".format(_invalid_content))

    if _validated["storage"] not in ("sd", "usb", "both"):
        raise ValueError(
            "{} isn't a valid sementeira storage".format(_validated["storage"])
        )

    _optional_params = ("netKey", "skelentonKey")
    for _optional_param in _optional_params:
        if params.get(_optional_param) is not None:
            _validated[_optional_param] = params[_optional_param]

    return _validated


@app.route("/install/setup", "POST")
@app.route("/install/setup/", "POST")
def install():
    def raise_exception(exc, status_code):
        msg = "Could not install Fuxico: {}".format(str(exc))
        bottle.abort(status_code, msg)

    try:
        validated = validate_install_params(bottle.request.params)
    except ValueError as exc:
        raise_exception(exc, 400)
    else:
        try:
            make_install(validated)
        except subprocess.SubprocessError as exc:
            raise_exception(exc, 500)
        else:
            bottle.response.content_type = "application/json"
            return bottle.HTTPResponse(status=201)


@app.error(404)
@app.error(400)
def client_error(error):
    print(error)
    return json.dumps({
        "status_code": error.status_code,
        "message": error.body,
    })


@app.error(500)
def internal_error(error):
    print(error)
    msg = error.body or "Fuxico com problemas."
    return json.dumps({
        "status_code": error.status_code,
        "message": msg,
    })


def setup_routing(app):
    app.route("/Shared/", "GET", shared_files)
    app.route("/Shared/<filepath:path>", "GET", shared_files)
    app.route("/Shared/upload/", "POST", upload_file)
    app.route("/Shared/<filepath:path>", "DELETE", delete_file)
    app.route("/hello", "GET", hello)


if __name__ == "__main__":
    setup_routing(app)
